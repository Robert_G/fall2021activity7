import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class mainTest {
    public static void main(String[] args) throws IOException{
        ListPractice ls = new ListPractice();
        List<String> s = ls.readStrings("strings.txt");
        System.out.println(s);
        List<String> upperS = ls.getUpperStrings(s);
        System.out.println(upperS);
        Files.write(Paths.get("upperstring.txt"), upperS);
        System.out.println(upperS.contains("HELLO"));
        System.out.println(upperS.contains("BYE"));
        ArrayList<Point> points = new ArrayList<Point>();
        points.add(new Point(5,4));
        points.add(new Point(30.6,1));
        points.add(new Point(4,500.15));
        Point target = new Point(5,4);
        System.out.println(points.contains(target));
        // String[] s = new String[]{"HELLO","Sup", "heYoo", "YAHOO"};
        // //ArrayList<String> words = new ArrayList<String>();
        // List<String> words = new ArrayList<String>();
        // System.out.println(words.size());
        // for(int i = 0; i < s.length; i++){
        //     words.add(s[i]);
        // }
        // System.out.println(words.size());
        // ListPractice ls = new ListPractice();
        // System.out.println(ls.countUpperCase(s));
        // String[] newS = ls.getUpperStrings(s);
        // for(String s2 : newS){
        //     System.out.println(s2);
        // }
        
    }
}

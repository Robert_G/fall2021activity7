public class Point {
    public double x;
    public double y;

    public Point(double x, double y){
        this.x = x;
        this.y = y;
    }

    // @Override
    // public boolean equals(Object other) {
    //     if(this.x == other.x) //x cannot be resolved or is not a field
    //     return false;
    // }

    @Override
    public boolean equals(Object other) {
        if(!(other instanceof Point)){
            return false;
        }
        Point test = (Point)other;
        return this.x == test.x && this.y == test.y;
        
    }

}

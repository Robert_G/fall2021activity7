import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ListPractice {
    public int countUpperCase(String[] strings) {
        int count = 0;
        for(String s : strings){
            if(s.matches("[A-Z]+") == true){
                count++;
            }
        }
        return count;
    }

    public String[] getUpperStrings(String[] strings){
        int count = 0;
        for(String s : strings){
            if(s.matches("[A-Z]+") == true){
                count++;
            }
        }
        String[] newArr = new String[count];
        int count2 = 0;
        for(String s : strings){
            if(s.matches("[A-Z]+") == true){
                newArr[count2] = s;
                count2++;
            }
        }
        return newArr;
    }

    public int countUpperCase(List<String> strings) {
        int count = 0;
        for(String s : strings){
            if(s.matches("[A-Z]+") == true){
                count++;
            }
        }
        return count;
    }

    public List<String> getUpperStrings(List<String> strings){
        List<String> newArr = new ArrayList<String>();
        for(String s : strings){
            if(s.matches("[A-Z]+") == true){
                newArr.add(s);
            }
        }
        return newArr;
    }

    public List<String> readStrings(String path) throws IOException{
        Path p = Paths.get(path);
        List<String> getLines = Files.readAllLines(p);
        List<String> splitLines = new ArrayList<String>();
        for(String a : getLines){
            String[] strArr = a.split(",");
            for(String b : strArr){
                splitLines.add(b);
            }
        }
        return splitLines;
    }
}